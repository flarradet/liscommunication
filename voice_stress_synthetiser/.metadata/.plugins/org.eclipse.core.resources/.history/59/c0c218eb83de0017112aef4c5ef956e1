/*
 * Created on 21.07.2005
 *
 * @author Felix Burkhardt
 */
package emofilt.plugins;

import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Element;

import emofilt.Constants;
import emofilt.ElemNotFoundException;
import emofilt.Emofilt;
import emofilt.Language;
import emofilt.ModificationPlugin;
import emofilt.Syllable;
import emofilt.util.Util;
import emofilt.Utterance;
import emofilt.gui.ModificationPanel;
import emofilt.gui.RateTypePanel;

/**
 * Modification Plugin
 * 
 * @author Felix Burkhardt
 */
public class FirstSylContour implements ModificationPlugin {
    private final String rateDesignator = "rate";

    private final String contourDesignator = "type";

    private String name = "";

    private String type = "";

    private final String initFileName = "init/firstSylContour_init.txt";

    private int defaultRate = 0;

    private HashMap initValues = null;

    private Logger debugLogger = null;

    private RateTypePanel mp = null;

    private boolean useGui = false;

    PropertyChangeListener pcl = null;

    public String getModificationType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public ModificationPanel getPanel() {
        return mp;
    }

    public void init(Logger logger, boolean useGui) {
        this.useGui = useGui;
        debugLogger = logger;
        debugLogger.debug(name + " initialisation, use GUI=" + useGui);
        try {
            initValues = Util.getValuesFromBufferedReader(new BufferedReader(new InputStreamReader(Emofilt.class.getResourceAsStream(initFileName))));
            name = (String) initValues.get("name");
            type = (String) initValues.get("type");
            defaultRate = Integer.parseInt((String) initValues
                    .get("defaultRate"));
            if (useGui) {
                Vector types = new Vector();
                types.add(Constants.CONTOUR_FALL);
                types.add(Constants.CONTOUR_STRAIGHT);
                types.add(Constants.CONTOUR_RISE);
                mp = new RateTypePanel(this, initValues, types);
            }
        } catch (Exception e) {
            e.printStackTrace();
            debugLogger.error(e.getMessage());
        }
    }

    public Utterance modify(Utterance utt, Element emotion, double globalRate, Language lang) {
      
        debugLogger.debug("ici");
        debugLogger.debug(globalRate);

    	
         // change the pitch 
    	
    	int rate = 0;
        String contour = "undefined";
        try {
            rate = Integer.parseInt(Util.getValueFromEmotion(emotion, name,
                    type, rateDesignator));
            contour = Util.getValueFromEmotion(emotion, name, type,
                    contourDesignator);
        } catch (ElemNotFoundException e) {
            rate = defaultRate;
        }
        if (rate == defaultRate) {
            return utt;
        }
    	// change the duration 

        int change = (int)((rate-defaultRate));
        
        // rate
        
        int rate_level = 0;
        try {
        	rate_level = Integer.parseInt(Util.getValueFromEmotion(emotion, name, type, "rate"));
        } catch (ElemNotFoundException e) {
        	rate_level = defaultRate;
        }
        if (rate_level == defaultRate) {
            return utt;
        }
        int change_level = (int)((rate_level-defaultRate));
        int GlobalRateFactor_level  = (int)(change_level*globalRate);
        rate_level = defaultRate + change_level + GlobalRateFactor_level;
        
        
          
        int GlobalRateFactor  = (int)(change*globalRate);
        rate = defaultRate + change + GlobalRateFactor;
        
        //Syllable syl = u.getFirstVoicedSyllable();
        //syl.changeContour(contour, rate);
        Utterance u = (Utterance) utt.clone();
        int i=0;
        Syllable s = (Syllable) u.getSyllables().elementAt(i);
        while(s.getWordNumber()==0)
        {
        	i++;
            s = (Syllable) u.getSyllables().elementAt(i);
			if (s.isVoiced()) {
				//s.changeContour(contour, rate);
                s.changeMeanF0(rate_level);

			}
        }
        
        
    	for (int i = 0 ; i < 2; i++) {
			Syllable s = (Syllable) u.getSyllables().elementAt(i);
			if (s.isVoiced()) {
				//s.changeContour(contour, rate);
                s.changeMeanF0(rate_level);

			}
		}
		
    	
    	
        return u;
    	
    }

    public String toString() {
        return "name: " + name + ", type: " + type;
    }

    public Element setEmotion(Element emotion) {
        String newRate = mp.getRateValue();
        Element returnElem = Util.setValueInEmotion(emotion, name, type,
                rateDesignator, newRate, String.valueOf(defaultRate));
        if (newRate.compareTo(String.valueOf(defaultRate)) != 0) {
            String newType = mp.getTypeValue();
            returnElem = Util.setValueInEmotion(returnElem, name, type,
                    contourDesignator, newType, "dummy");
        }
        return returnElem;
    }

    public void setGui(Element emotion) {
        int rate = defaultRate;
        String contourType = "undefined";
        try {
            rate = Integer.parseInt(Util.getValueFromEmotion(emotion, name,
                    type, rateDesignator));
            contourType = Util.getValueFromEmotion(emotion, name, type,
                    contourDesignator);
        } catch (ElemNotFoundException e) {
            debugLogger.debug(e.getMessage());
        }
        mp.setRateValue(new Integer(rate));
        mp.setTypeValue(contourType);
    }

    public void setGuiDefault() {
        mp.setDefault();
    }

    public void setPropertyChangeListener(PropertyChangeListener pcl) {
        this.pcl = pcl;
        mp.setPropertyChangeListener(pcl);
    }
}