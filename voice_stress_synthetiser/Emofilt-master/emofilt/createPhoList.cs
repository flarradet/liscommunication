using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

class TestClass
{


    static void Main(string[] args)
    {

        string text = args[0];
        createPhoList("us2", text);


    }


    static void createPhoList(string languageName, string text)
    {

        String path = "C:\\Users\\fanny\\Documents\\final_setup\\voice_stress_synthetiser\\Emofilt-master\\emofilt\\";

        string firstLine = "";
        string[] words = text.Split(' ');
        string phoneme = path + "test/mypho.pho";
        String phoFile = path + "phoList.txt";
        List<String> strList = new List<String>();

        for (int wInt = 0; wInt < words.Length; wInt++)
        {
            if (words[wInt] != "")
            {
                string res = doCommand(@"C:\Program Files (x86)\eSpeak\command_line\espeak.exe", "-q -v mb-" + languageName + " --pho \"" + words[wInt] + "\"");
                string[] lines = res.Split('\n');
                int count = 0;

                foreach (string line in lines)
                {
                    if (line.Split('\t')[0] != "_")
                    {
                        strList.Add((line.Split('\t')[0]).Split('\n')[0] + ";");


                        count = count + 1;
                    }
                }
                strList.RemoveAt(strList.Count - 1);
                count = count - 1;


                strList[strList.Count - 1] = strList[strList.Count - 1].Substring(0, strList[strList.Count - 1].Length - 1);

                strList.Add("\n");

                if (wInt == words.Length - 1)
                    count = count + 2;
                firstLine = firstLine + count + ";";

            }
        }
        strList.RemoveAt(strList.Count - 1);
        strList.RemoveAt(strList.Count - 1);
        strList.Add(";_;_");


        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(phoFile, false);
        writer.Write(firstLine.Substring(0, firstLine.Length - 1) + "\n");

        for (int i = 0; i < strList.Count; i++)
        {
            writer.Write(strList[i]);
        }

        writer.Close();


    }

    static string doCommand(string cmd, string arguments)
    {
        System.Diagnostics.Process p = new System.Diagnostics.Process();
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.CreateNoWindow = true;


        // Correct way to launch a process with arguments
        p.StartInfo.FileName = cmd;
        p.StartInfo.Arguments = arguments;
        p.StartInfo.WorkingDirectory = "C:\\Users\\fanny\\Documents\\final_setup\\voice_stress_synthetiser\\Emofilt-master\\emofilt";
        p.Start();

        string q = "";
        while (!p.HasExited)
        {
            q += p.StandardOutput.ReadToEnd();
        }
        return (q);
    }
}