﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;
using System.IO;
using System.Text;
using System.Collections.Generic;

public class keyboard_manager_interface : MonoBehaviour
{

	public InputField focus;
	public GameObject[] panels;
	public GameObject[] keys;
	public GameObject[] specialKeys;

	[HideInInspector]
	public bool isActive = false;
	[HideInInspector]
	public bool capsEnabled = false;
	private string gender, emotion;
	public Text voiceBtn;
	String[] emotions = new String[] { "happy", "sad", "angrySad", "neutral" };
	public Sprite[] emotionsImagesSelected;
	public Sprite[] emotionsImagesUnselected;
	public Button[] emotionsButtons;
	public autoCompletion ac;
	public bool isActivated;

	String lastSentence="";
	int position;

	

    Color[] buttonColors = new Color[] { new Color(0.22F, 0.22F, 0.22F), new Color(0.59F, 0F, 0F) };//"383838FF";"960000FF"]
    bool isStressImage;

    bool finished;
    void Start()
    {
    	Time.timeScale = 1; 

    	gender = "Male";
    	emotion = "happy";
    	isActivated = true;


    
    	ac.updateProposals(focus.text,position);
    	/*for(int i=3; i<6;i++)
    	{
    		specialKeys[i].GetComponent<Button>().interactable= false;
    	}*/
    	position=0;
    	finished  = false;

    }

    private void Update()
    {

    	focus.Select();
    	focus.caretPosition=position ;
    	transform.localScale = new Vector3(0.0007f * Screen.width - 0.0302f, 0.0012f * Screen.height , 1.0f);
    }

    public void changeEmotion(int emotionNb)
    {
    	emotion = emotions[emotionNb];
    	for (int i = 0; i < 4; i++)
    	{
    		if (i != emotionNb)
    		emotionsButtons[i].image.overrideSprite = emotionsImagesUnselected[i];
    		else
    		emotionsButtons[i].image.overrideSprite = emotionsImagesSelected[i];

    	}
    }
    
        public void SetFocus(InputField i)
        {
        	focus = i;
        }

        public void SetActiveFocus(InputField i)
        {
        	focus = i;
        	SetActive(true);
        	focus.MoveTextEnd(true);
        }

        public void WriteKey(Text t)
        {
        	//if (isActivated)
        //	{

        		if(position == focus.text.Length)
        		{
        			focus.text +=    t.text ;
        		}
        		else{
        			focus.text =   focus.text.Substring(0, position) + t.text + focus.text.Substring( position,focus.text.Length-position);
        		}

        	
        	   position++;
        	   focus.caretPosition=position ;

        	   ac.updateProposals(focus.text,position);
          //  }

        }


        public void WriteSpecialKey(int n)
        {

        	if (n == 14 || isActivated)
        	{
        		float threshold;
        		switch (n)
        		{
                case 0: // delete letter 
                	deleteLetter();
                	break;
                case 1:  // play 
 	                say(focus.text);
                	break;
                case 2:
                	SwitchCaps();
                	break;
                case 3:
                	SetActive(false);
                	break;
                case 4:
                	SetKeyboardType(1);
                	break;
                case 5:
                	SetKeyboardType(2);
                	break;
                case 6: // previous letter 
                    if(position>0)
                    {
                        position --;
                        focus.caretPosition=position ;
                        ac.updateProposals(focus.text,position);
                    }
                	break;
                case 7: //next position
                    if(position!= focus.text.Length)
                    {
                        position ++;
                        focus.caretPosition=position ;                        
                        ac.updateProposals(focus.text,position);
                    }
                	break;
                case 8:
                	SetKeyboardType(0);
                	break;
                case 9: //threshold
                	threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main + 0.25f;
                	GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                	GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                	break;
                case 10: // threshold
                	threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main - 0.25f;
                	threshold = Math.Max(0.25f, threshold);
                	GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                	GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                	break;
                case 11: // change gender
	                if (gender == "Male")
	                {
	                	gender = "Female";
	                	voiceBtn.text = "Femminile";
	                }
	                else
	                {
	                	gender = "Male";
	                	voiceBtn.text = "Maschio";
	                }
	                break;
                case 12: // display/hide ball
	             /*   if (GameObject.Find("ballBtn").GetComponentInChildren<Text>().text == "display ball")
	                {
	                	GameObject.Find("ballBtn").GetComponentInChildren<Text>().text = "hide ball";
	                	GameObject.Find("Sphere").GetComponent<Renderer>().enabled = true;
	                }
	                else
	                {
	                	GameObject.Find("ballBtn").GetComponentInChildren<Text>().text = "display ball";
	                	GameObject.Find("Sphere").GetComponent<Renderer>().enabled = false;
	                }*/
                    GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall= !GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall;
                    if(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall)
                        GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.red;
                    else
                        GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.white;
                     GameObject.Find("eyeTracker").GetComponent<eye_tracker>().gazePlot.SetActive(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall);

	                break;
                case 13: // clear 
	                clear();
	                break;
                case 14: // sleep mode 
	                //isActivated = !isActivated;
	                for (int i=0;i< 3; i++)
	                {
	                	specialKeys[i].GetComponent<Button>().GetComponent<Image>().color = buttonColors[Convert.ToInt32(!isActivated)];
	                }

	                break;
                case 15: // delete word
	                deleteWord();
	                break;
                case 16: //repeat
	                say(lastSentence);
	                break;

            }
        }
      //  remplaceText();
    }

    public void SetActive(bool b)
    {
    	if (b)
    	{
    		if (!isActive)
    		{
    			gameObject.GetComponent<Animator>().Rebind();
    			gameObject.GetComponent<Animator>().enabled = true;
    		}
    	}
    	else
    	{
    		if (isActive)
    		{
    			gameObject.GetComponent<Animator>().SetBool("Hide", true);
    		}
    	}

    	isActive = b;
    }

    public void SetCaps(bool b)
    {
    	if (b)
    	{
    		foreach (GameObject go in keys)
    		{
    			Text t = go.transform.Find("Text").GetComponent<Text>();
    			t.text = t.text.ToUpper();
    		}
    	}
    	else
    	{
    		foreach (GameObject go in keys)
    		{
    			Text t = go.transform.Find("Text").GetComponent<Text>();
    			t.text = t.text.ToLower();
    		}
    	}
    	capsEnabled = b;
    }

    public void SwitchCaps()
    {
    	SetCaps(!capsEnabled);
    }


    public void SetKeyboardType(int n)
    {
    	switch (n)
    	{
    		case 0:
    		panels[0].SetActive(true);
    		panels[1].SetActive(false);
    		panels[2].SetActive(false);
    		break;
    		case 1:
    		panels[0].SetActive(false);
    		panels[1].SetActive(true);
    		panels[2].SetActive(false);
    		break;
    		case 2:
    		panels[0].SetActive(false);
    		panels[1].SetActive(false);
    		panels[2].SetActive(true);
    		break;
    	}

    	ac.updateProposals(focus.text,position);
    }

    public void say(string text)
    {
// ENABLE REPEAT 
    	for(int i=3; i<6;i++)
    	{
    		specialKeys[i].GetComponent<Button>().interactable= true;
    	}

    	lastSentence= text ;
        /*try
        {

            if (GameObject.Find("socketManager").GetComponent<socketManager>().isStressed)
            {
                emotion = "angrySad";
            }
            else
            {
                emotion = "happy";
            }
            }catch(Exception e)
            {
                emotion = "happy";
            }
    */

        /*
        string language = "it3";
        if (gender != "Male")
        {
            language = "it4";
        }
        */
        ///////////////////

        // version emofilt
        ///////////////////

        /*      string language = "en1";
              if (gender != "Male")
              {
                  language = "us1";
              }

        */
        /*
        print("entre ici ");
            String language = "us2";
        emotion = "sad";
              string localisation_mbrola = "C:\\Users\\fanny\\Documents\\OpenSource\\sightWeb\\voice_stress_synthetiser\\voices_mbrola\\";

              doCommand("/C  echo '" + text + "' | espeak -v mb-" + language + " --pho --phonout=test/mypho.pho");
            createPhoList();
              doCommand("/C java -jar emofilt.jar -cf emofiltConfig_windows.ini -if test/mypho.pho -voc " + language + " -e " + emotion + " -of tmp_e.pho");
               doCommand("/C phoplayer database=" + localisation_mbrola + language + "\\" + language + " tmp_e.pho");
      
        */

        ///////////////////

        // normal version
        ///////////////////
        
        GameObject.Find("speech").GetComponent<WindowsVoice>().speak(focus.text, "Language=410;Gender=Female");

    }

    public void doCommand(string cmd)
    {
    	System.Diagnostics.Process p = new System.Diagnostics.Process();
    	p.StartInfo.UseShellExecute = false;
    	p.StartInfo.RedirectStandardOutput = true;
    	p.StartInfo.CreateNoWindow = true;

        // Correct way to launch a process with arguments
    	p.StartInfo.FileName = "CMD.exe";
    	p.StartInfo.Arguments = cmd;
    	p.StartInfo.WorkingDirectory = "C:\\Users\\flarradet\\Documents\\phd\\voice_stress_synthetiser\\Emofilt-master\\emofilt";
    	p.Start();


    	string output = p.StandardOutput.ReadToEnd();
    	p.WaitForExit();
    }

    public void writeWord(int index)
    {

    //	if (isActivated)
    //	{

    		if(focus.text=="")
    		{
    			focus.text= ac.proposals[index].text;
    			position = ac.proposals[index].text.Length+1;

    			focus.text = focus.text+" ";
    			focus.caretPosition=position ;


        	}/*
        	else if( focus.text[focus.text.Length -1]== ' ')
        	{
        		focus.text= focus.text + ac.proposals[index].text;
        		position = position + ac.proposals[index].text.Length+1;

        	}
        	else{
            String[] words = focus.text.Split(' ');

            int toCut = words[words.Length - 1].Length;
            String t = focus.text.Substring(0, focus.text.Length - toCut);
            String b = ac.proposals[index].text;
            focus.text = focus.text.Substring(0, focus.text.Length - toCut) + ac.proposals[index].text;
            position = position + ac.proposals[index].text.Length- toCut+1;

            }*/
            else{
            	String focusedWord =	ac.getFocusedWord(focus.text,position);

            	focus.text= focus.text.Substring(0, position - focusedWord.Length )+ ac.proposals[index].text + focus.text.Substring(position,focus.text.Length - position);
            	position= position + ac.proposals[index].text.Length - focusedWord.Length;
            	if(focus.text.Length==position ||  focus.text[position]!=' ')
            	{    focus.text = focus.text+" ";
            	position++;
            }
     //   }

    	focus.caretPosition=position ;

        ac.updateProposals(focus.text,position);
        
    }
      //  remplaceText();
}
    /*
    public void remplaceText(){

		if(focus.text!="")
		{
			focus.text= focus.text;
		
			if(focus.text[focus.text.Length -1]== ' ')
			{
				focus.text= focus.text.Substring(0, focus.text.Length - 1) + "_";
			}
		}
		}*/
		public void deleteLetter()
		{
			if (!focus) { return; }
			if (focus.text.Length > 0)
			{
                        //focus.text = focus.text.Substring(0, focus.text.Length - 1);
				focus.text= focus.text.Substring(0, position-1 )+ focus.text.Substring(position,focus.text.Length - position);
				position --;

			}
    		focus.caretPosition=position ;

			ac.updateProposals(focus.text,position);

		}
		public void deleteWord()
		{
			if(focus.text!="")
			{
				String[] words = focus.text.Split(' ');
				int cpt=0;
				int j=0;
				while(j<words.Length && cpt < position)
				{
					cpt=cpt+words[j].Length;
                    if(j!=0) //count the space in between words
                    cpt++;
                    j++;
                }
                int toRemove;
                if(j>0)
                {
                	toRemove= words[j-1].Length;

            // if the focus character is a space, delete it and the last word
                	if(toRemove ==0)
                	{
                		deleteLetter();
                		deleteWord();
                	}else{

                		focus.text= focus.text.Substring(0, cpt-toRemove )+ focus.text.Substring(cpt,focus.text.Length-cpt );

          //  focus.text = focus.text.Substring(0, focus.text.Length  - toRemove);
                		position = cpt - toRemove;
                	}

                }
                focus.caretPosition=position ;

                ac.updateProposals(focus.text,position);
            }
        }

        public void clear()
        {
        	focus.text = "";
        	position =0;
           	focus.caretPosition=position ;

        	ac.updateProposals(focus.text,position);

        }
    public void backToMenu()
    {

      //  Application.LoadLevel("menu"); 
        SceneManager.LoadScene("menu");
        GameObject.Find("settings").GetComponent<settings>().savePreferencesSpeech(); 
    }
   
}
