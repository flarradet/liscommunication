﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;

using System.IO;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;


public class Util : MonoBehaviour {
	public static bool isCursorDisplayed;
    public static Vector3 _historicPoint_browser, _historicPoint_gaze;
    private static bool _hasHistoricPoint=false;
    public enum ButtonStatus { Normal, Hover,Pressed };
   // public static float y = 0.0133f*Screen.height+45.0f;

     public static Vector2[]  positionsBall = new Vector2[5] {new Vector2(0,0),new Vector2(-120,0.0133f*Screen.height+45.0f),new Vector2(120,0.0133f*Screen.height+45.0f), new Vector2(-120,-(0.0133f*Screen.height+45.0f)),new Vector2(120,-(0.0133f*Screen.height+45.0f))};
  //  public static Vector2[]  indexes = new Vector2[5] {new Vector2(0,Screen.width),new Vector2(Screen.width,Screen.height), new Vector2(0,0),new Vector2(Screen.width,0)};

            [DllImport("user32.dll")]
    public static extern int ShowCursor(bool display);


    public static List<System.Drawing.PointF> removeOutsiders(List<System.Drawing.PointF> lookingPoints, int initialCount)
    {
        //get the center, remove the farsest, restart again until the radius is < ? 
        // for the radial menu, are 95% of the points within this radius? ( you can stop once you removed too much points )
        PointF center;
        float radius;
        Geometry.FindMinimalBoundingCircle(lookingPoints, out  center, out  radius);
      if(radius < 15)
     //  if(lookingPoints.Count < 0.80*initialCount)
       {
            return lookingPoints;
        }
        else
        {

           // lookingPoints.OrderBy(x => GetDistance(x,center));
            //lookingPoints.RemoveAt(0);
            float max=-1;
            int indexMax=-1;
            for(int i=0; i< lookingPoints.Count ; i++)
            {
                float d = GetDistance(lookingPoints[i],center);
                if(max< d)
                {
                    max = d;
                    indexMax=i;
                }
            }
            lookingPoints.RemoveAt(indexMax);

            return removeOutsiders(lookingPoints,initialCount);
        }
    }

    public static float GetDistance(PointF point1, PointF point2)
    {
        //pythagorean theorem c^2 = a^2 + b^2
        //thus c = square root(a^2 + b^2)
        float a = (float)(point2.X - point1.X);
        float b = (float)(point2.Y - point1.Y);

        return Mathf.Sqrt(a * a + b * b);
    }
      public static float GetDistance(Vector2 point1, Vector2 point2)
    {
       return GetDistance(new PointF(point1.x,point1.y), new PointF(point2.x,point2.y));
    }


    public static Vector3[] Smoothify3(Vector3 point,float FilterSmoothingFactor )
    {
        if (!_hasHistoricPoint)
        {
            _hasHistoricPoint = true;
            _historicPoint_gaze=point;
            _historicPoint_browser=point;

            return  new Vector3[2]{point, point};
        }
                float smoothWithDistance ;

        // smoothin according to the distance


        smoothWithDistance = -1.0f*(Util.GetDistance(_historicPoint_browser,point)/400.0f -1.0f) *0.1f +0.8f; // last + the bigger the more filtered at the end 
        smoothWithDistance=Mathf.Min(smoothWithDistance,0.98f);            
       
       // print(*);

    	var smoothedPointDistance = new Vector3(
    		point.x * (1.0f - smoothWithDistance) + _historicPoint_browser.x * smoothWithDistance,
    		point.y * (1.0f - smoothWithDistance) + _historicPoint_browser.y * smoothWithDistance,
    		point.z * (1.0f - smoothWithDistance) + _historicPoint_browser.z * smoothWithDistance);
        var smoothedPoint = new Vector3(
            point.x * (1.0f - FilterSmoothingFactor) + _historicPoint_gaze.x * FilterSmoothingFactor,
            point.y * (1.0f - FilterSmoothingFactor) + _historicPoint_gaze.y * FilterSmoothingFactor,
            point.z * (1.0f - FilterSmoothingFactor) + _historicPoint_gaze.z * FilterSmoothingFactor);

        //_historicPoint=smoothedPoint;
        _historicPoint_gaze=smoothedPoint;
        _historicPoint_browser=smoothedPointDistance;
        Vector3[] v = new Vector3[2]{smoothedPointDistance, smoothedPoint};
    	return v;
    }



    public static  List<System.Drawing.PointF>  copyList( List<System.Drawing.PointF> lookingPoints)
    {
		 List<System.Drawing.PointF> l= new  List<System.Drawing.PointF>();
		 for( int i=0; i< lookingPoints.Count;i++)
		 {
		 	l.Add(new PointF(lookingPoints[i].X, lookingPoints[i].Y));
		 }
		 return l;
    }

    public static void displayCursor(bool display)
    {
    	 if(display)
        {
            while(ShowCursor(true) <0){} // display cursor

        }
        else
        {
          //  while(ShowCursor(false) >-1){}  // hide cursor
            
        }
        isCursorDisplayed=display;
    }

       public static Vector3 calculateFixedPoint( Vector3 gazePosition)
    {


    	Vector2[] offsets;
        Vector2 meanOffsets;

		if( GameObject.Find("settings"))
        {
            settings set = GameObject.Find("settings").GetComponent<settings>() ;
            offsets = set.sp.offsets;
            meanOffsets = set.sp.meanOffsets;

        }
        else  // if we run the browser directly, not throught the whole system 
        {
            offsets= new Vector2[9]{new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0)};
            meanOffsets = new Vector2(0,0);
        }

     /*   print("width"+Screen.width);
        print("width"+Screen.height);

        print("index is"+index);
        print("gaze"+ gazePosition);*/


        int index,sidex,sidey;    
        float h = Screen.height;
       	float w = Screen.width;
  /*
        if( gazePosition.x < w/3 && gazePosition.y > 2*h/3)
            index =1;
        else if( (gazePosition.x > w/3 && gazePosition.x < 2*w/3 ) && gazePosition.y > 2*h/3)
            index=2;
        else if( (gazePosition.x > 2*w/3 ) && gazePosition.y > 2*h/3)
            index=3;
        else if( (gazePosition.x  < w/3 ) && (gazePosition.y > h/3 && gazePosition.y < 2*h/3 ))
            index=4;
        else if((gazePosition.x > w/3 && gazePosition.x < 2*w/3 ) && (gazePosition.y > h/3 && gazePosition.y < 2*h/3 ))
            index=0;
        else if((gazePosition.x > 2*w/3 ) && (gazePosition.y > h/3 && gazePosition.y < 2*h/3 ))
            index=5;
        else if(gazePosition.x < w/3 && (gazePosition.y < h/3))
            index=6;
        else if( (gazePosition.x > w/3 && gazePosition.x < 2*w/3 ) && (gazePosition.y < h/3))
            index=7;
        else
            index=8;
*/
/*
            print("x="+gazePosition.x);
            print("y="+gazePosition.y);
            print("w"+Screen.width/2);
            print("h"+Screen.height/2);
        if( gazePosition.x < w/2 && gazePosition.y > h/2){
            index =1;
            sidex= 2;
            sidey=3;
        }
        else if( (gazePosition.x > w/2 ) && gazePosition.y > h/2){
            index=2;
            sidex=1;
            sidey=4;
        }
        else if(gazePosition.x < w/2 && (gazePosition.y < h/2)){
            index=3;
            sidex=4;
            sidey= 1;
        }
        else{
        	index=4;
        	sidex=3;
        	sidey=2;
        }
*/


        //     Vector3 fixedPoint_tpm =   new Vector3(gazePosition.x - offsets[index][0] , gazePosition.y - offsets[index][1] ,gazePosition.z);

          //  Vector2 percent =  new Vector2();
          /* percent.x = ((((2*centeredPos.x)/Screen.width)-1) )*Mathf.Pow((-1),index) ; // -1 for corners 1 and 3

            if(index == 1 || index==2) // -1 for corners 1 and 2
               percent.y = (((2*centeredPos.y)/(Screen.height))-1)  ;
            else
               percent.y = (((2*centeredPos.y)/(Screen.height))-1)*(-1) ; 
*/

/*
        float indexToPoint = GetDistance( indexes[index],gazePosition);
        float sidexToPoint = GetDistance(indexes[sidex],gazePosition);
        float sideyToPoint = GetDistance(indexes[sidey],gazePosition);
        float centerToPoint = GetDistance(indexes[0],gazePosition);

        print( "index"+indexToPoint );
        print( "sidex"+sidexToPoint );
        print( "sidey"+sideyToPoint );
        print( "center"+percentCenter+";"+centerToPoint );
        print("gaze"+gazePosition);

        float total= indexToPoint+sidexToPoint+sideyToPoint+centerToPoint;

// inverse so that shortest distance equal bigger weight 
         indexToPoint = total - indexToPoint;
         sidexToPoint = total - sidexToPoint;
         sideyToPoint = total - sideyToPoint;
         centerToPoint = total - centerToPoint;

         total= indexToPoint+sidexToPoint+sideyToPoint+centerToPoint;


        float percentIndex = indexToPoint/total;
        float percentSidex = sidexToPoint/total;
        float percentSidey = sideyToPoint/total;
        float percentCenter = centerToPoint/total;


        //print( "index"+indexToPoint/total );
        //print( "sidex"+sidexToPoint/total );
        //print( "sidey"+sideyToPoint/total );
        //print( "center"+centerToPoint/total );
        print( "index"+index+";"+percentIndex+";"+indexToPoint );
        print( "sidex"+sidex+";"+percentSidex+";"+sidexToPoint );
        print( "sidey"+sidey+";"+percentSidey+";"+sideyToPoint );
        print( "center"+percentCenter+";"+centerToPoint );
        print("gaze"+gazePosition);

        float x =  gazePosition[0] - percentIndex* offsets[index][0] - percentSidex* offsets[sidex][0] - percentSidey* offsets[sidey][0] - percentCenter* offsets[0][0] ; 
        float y =  gazePosition[1] - percentIndex * offsets[index][1] - percentSidex* offsets[sidex][1] - percentSidey* offsets[sidey][1] - percentCenter* offsets[0][1] ; 

		//	float x = gazePosition[0] - offsets[index][1]
		//	float y = gazePosition[1]  - (offsets[index][1]*0.5f + offsets[index][1]*0.5f*percent.x +(offsets[sidex][1]*0.5f)*(1.0f-percent.x))*percent.y;

		//	float x = centeredPos[0] - offsets[index][0]*percent.x;
		//	float y = centeredPos[1]  - offsets[index][1]*percent.y;

        Vector3 fixedPoint =   new Vector3(x ,y,gazePosition.z);

        //return fixedPoint_tpm;
        //return gazePosition;
        return fixedPoint;
        */
/*

        float total=0;
        float[] percents = new float[5];
        for(int i=0;i<4;i++)
        {
            percents[i]=  GetDistance( indexes[i],gazePosition);
            total=total+ percents[i];
        }
        float total2=0;
        for(int i=0;i<4;i++)
        {
            percents[i]= total - percents[i];
            total2= total2+percents[i];
        }


        float x =  gazePosition[0] ;
        float y =  gazePosition[1];  


        for(int i=0;i<4;i++)
        {
            percents[i]= percents[i]/total2;
            print("i="+i+"; percent:"+percents[i]);
            x= x- percents[i]* offsets[i][0] ;
            y= y- percents[i]* offsets[i][1] ;

        }

        Vector3 fixedPoint =   new Vector3(x ,y,gazePosition.z);
        return fixedPoint;

*/

                /*    
            print("x="+gazePosition.x);
            print("y="+gazePosition.y);
            print("w"+Screen.width/2);
            print("h"+Screen.height/2);
*/


        Vector3  pointCentered = new Vector3(gazePosition.x- meanOffsets[0]   ,gazePosition.y - meanOffsets[1] ,gazePosition.z);

        if( pointCentered.x < w/2 && pointCentered.y > h/2){
            index =1;
        }
        else if( (pointCentered.x > w/2 ) && pointCentered.y > h/2){
            index=2;
        }
        else if(pointCentered.x < w/2 && (pointCentered.y < h/2)){
            index=3;
        }
        else{
            index=4;
        }

        Vector2 percent =  new Vector2();

        percent.x = Mathf.Abs((((2*pointCentered.x)/Screen.width)-1)); 
        percent.y =  Mathf.Abs(((2*pointCentered.y)/(Screen.height))-1);

        float x =  pointCentered[0] - percent.x * offsets[index][0] -  (1-percent.x)* offsets[0][0] ; 
        float y =  pointCentered[1] - percent.y * offsets[index][1] -  (1-percent.y)* offsets[0][1] ; 

        Vector3   pointFixed = new Vector3(x ,y,gazePosition.z);



      //  Vector3  pointFixed = new Vector3(pointCentered.x -offsets[index][0]  ,pointCentered.y - offsets[index][1],gazePosition.z);

       //pointFixed = new Vector3(gazePosition.x-offsets[index][0]  ,gazePosition.y- offsets[index][1],gazePosition.z);
    //Vector3   pointFixed = new Vector3(gazePosition.x ,gazePosition.y,gazePosition.z);


        return pointFixed;
        
      //  Vector3   pointFixed = new Vector3(gazePosition.x ,gazePosition.y,gazePosition.z);
     //   return pointFixed;

    }

    
}

