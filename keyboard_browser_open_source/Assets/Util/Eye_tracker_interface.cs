using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;
using UnityEngine.SceneManagement;
using System;
public class Eye_tracker_interface : MonoBehaviour{

	//variables defined in GUI 
	public UnityEngine.UI.Image timer ;
	public GameObject gazePlot ;
	public Camera camera ;
	public Camera cameraGUI ;
	public Camera cameraOverGUI ;
///////////////////////////////////////////

    public bool test{ get; set; }

	public float begin_time{ get; set; }
	public float begin_time_new{ get; set; }
    public float begin_time_out{ get; set; }
	public string name { get; set; }
	public string name2 { get; set; }
	public string oldname { get; set; }
	public string newName { get; set; }
	public float time_to_wait { get; set; }
    [HideInInspector]
	public float time_to_wait_main;
	public bool empty { get; set; }
	public Vector2[] offsets { get; set; }
	public bool newOffset { get; set; }
	public Vector3 meanOffsets { get; set; }
	public int index { get; set; }

	[HideInInspector]
	public Vector3 pointFixed, smoothedPoint , smoothedFixedPoint ,toRay;
	public Vector3 _historicPoint { get; set; }
	public bool _hasHistoricPoint{ get; set; }
	public Vector3 gazePosition { get; set; }
	public RaycastHit hit ;
	public Ray camRay;
	public float time_ignore{ get; set; }
	public float time_ignore_main{ get; set; }
     float distance = 0;
      Vector2 lastGaze ;

	[HideInInspector]
    public MyButton lastImage,hoverImage;
    [HideInInspector]
    public bool sleepMode, displayBall,startTimeOut;
    [DllImport("user32.dll")]
	public static extern int ShowCursor(bool display);


	public void setup()
	{
   
       // timer = GameObject.Find("timer").GetComponent<UnityEngine.UI.Image>();
		timer.enabled = false;
		begin_time= Time.time;
		name = "";
		name2 = "";
    	empty = false;

		camera.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, -1000);
		camera.fieldOfView = 0.0535f * Screen.height + 1f;
		if(cameraGUI)
		{
			cameraGUI.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, -1000);
			cameraGUI.fieldOfView = 0.0535f * Screen.height + 1f;
		}
		if(cameraOverGUI)
		{
			cameraOverGUI.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, -1000);
			cameraOverGUI.fieldOfView = 0.0535f * Screen.height + 1f;
		}


        // by default we donot display the gazeBall and we are not in sleep mode
        displayBall=false;
        sleepMode=false;
        time_ignore_main= 0.5f;
		getDataFromPlayerPrefs();

	}




	public void gazeAction()
	{
        // if we put any key in green to aknowledge it, repaint it in its original color 
        if (lastImage != null&& lastImage.wait_color>0)
        {
            lastImage.wait_color--;
        }
        else if (lastImage != null)
        {
            lastImage.setStatus(Util.ButtonStatus.Normal); 
            lastImage=null;
        }
            
      bool  useMouse = false;
       Vector2 gaze;

      if (useMouse)
              gaze= Input.mousePosition;
      else
           gaze= TobiiAPI.GetGazePoint().Screen;


        gazePosition = new Vector3(gaze.x, gaze.y, 0.0f);

		if (useMouse || TobiiAPI.GetGazePoint().IsValid )
		{

            if(lastGaze!= null)
            {
 
                distance +=  Util.GetDistance(lastGaze, gaze);
            }

            lastGaze= gaze;

			getFinalPoint();
			ActionsWhenPointValid();


            // Perform the raycast and if it hits something on the UI layer ( buttons and keyboard) 
            if (touchSomething("UI") )
            {
            	empty = false;
            	getNewName();

                // if we are looking at the same object as before       
            	if (name == newName)
            	{        
            		name2 = "";
            		ActionWhenLookAtSomething();
            		getTime_to_wait();
                     // if its a proposal key or a selection time key and the time is > threshold with a minimum of 0.75 or if its another key and the time is > threshold     
                    if (Time.time - begin_time>  time_to_wait)
            		{
            			oldname = name;
            			doActionButton();
                        forgetKey();

                    }  // if we are looking at the same key as before but the time is not over yet and if we are not in sleep mode or if we are in sleep mode but it is the sleep mode button , update the timer display 
                    else 
                    {
                    	timer.fillAmount = (Time.time - begin_time-time_ignore) / (time_to_wait-time_ignore);
                    }
                } // if we are not looking at the same key as before ( or we ar looking at the same key as before but since not long, the goal is that if you quickly glance at another key but quickly come back to the other key we dont reset the timer )
                else
                {
                    checkIfMustForgetKey();
                   if(hoverImage!=null ){
                        hoverImage.setStatus(Util.ButtonStatus.Normal); 
                        hoverImage=null;
                    }

                    // if we look at an interactable button, paint it black
                    if(hit.collider && !sleepMode){
                        MyButton b = hit.collider.gameObject.GetComponent<MyButton>();
                        if(b!=null && b.interactable  ){
                               // hoverImage = new MyButton(b.GetComponent<UnityEngine.UI.Image>());
                               // hoverImage.hover();
                            hoverImage= b;
                        ///    if(hoverImage!=null && !(hoverImage==lastImage && lastImage.status == Util.ButtonStatus.Pressed))   {
                               hoverImage.setStatus(Util.ButtonStatus.Hover); 
                          //  }
                        }
                    }

                    // if we were already looking at this new key 
                	if(newName== name2  )
                	{
                		getTime_ignore();
                            //and that the time since we started looking at this new key is big enough, consider the key for real 
                		if ( Time.time - begin_time_new > time_ignore)
                		{
                                // reset the time 
                			begin_time = begin_time_new;
                			name = newName;
                			startConsideringKey();
                         	timer.fillAmount = 0;  ////
                		}
                	} // if this button is interactable
                	else if(isInteractable())
                	{
                        // wait to consider it , start a temporary timer 
                		begin_time_new = Time.time;
                		name2 = newName;
                	}
                     // if its a totaly new key we are looking at and this key is not disabled
                }
            }
            else // if we are looking at nothing 
            {  

            	    lookNothing();
            }
        }else// if the eye is not detected 
        {
         //   GameObject.Find("thresholdText").GetComponent<Text>().color = UnityEngine.Color.red;
        }
    }

    public virtual void getFinalPoint(){}


    public virtual void doActionButton(){
        GameObject.Find("settings").GetComponent<settings>().lastTimeUsedTracker = DateTime.Now; 
    }

    public virtual void startConsideringKey(){}
    public virtual void ActionsLookNothing(){}
    public virtual bool touchSomething(string mask){
    	return Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask(mask));
    }
    public virtual void getNewName(){
    	newName= hit.collider.gameObject.name;
    }
    public virtual void ActionWhenLookAtSomething(){
    }
    public virtual void getTime_to_wait(){}
    public virtual void getTime_ignore(){
		time_ignore = time_ignore_main;
    }
    public void lookNothing()
    {
        
        checkIfMustForgetKey();

                if(hoverImage!=null )   {
                        hoverImage.setStatus(Util.ButtonStatus.Normal);
                        hoverImage=null;
                }
    	                  // if we were already pointing nothing before
            	if(empty)
            	{
                    // if we are looking at nothing for long enough, considerate it as a command and reset the timer and the selected key
            		if (Time.time - begin_time_new > time_ignore)
            		{
                        forgetKey();
                        ActionsLookNothing();
                    }
                } // if we were looking at something before
                else
                { 
                    // start a temporary timer to see how long we are looking at nothing for 
                	begin_time_new = Time.time;
                	empty = true;
                }
    }
    public virtual void getDataFromPlayerPrefs(){}
    public virtual void ActionsWhenPointValid(){
    	if (touchSomething("Background"))
			{
                gazePlot.transform.position =  hit.point;
            }
    }
    public bool isInteractable()
    {
        if(hit.collider==null)
            return true;
        else if(hit.collider.gameObject==null)
            return true;
        else if(hit.collider.gameObject.GetComponent<Button>()==null)
            return true;
        else
            return hit.collider.gameObject.GetComponent<Button>().interactable;
    }

    public void forgetKey()
    {


        name = "";
        timer.enabled = false; // hide the timer display 
        timer.fillAmount = 0;
        startTimeOut=false;
    }





    public float thresholdModify(ref float time_to_wait , Text t,float toAdd)
    {
        float threshold;
        threshold = time_to_wait + toAdd;
        threshold = Math.Max(0.25f, threshold);

        time_to_wait = threshold;
        t.text = t.text.Split(':')[0]+": " + threshold;
        return threshold;
    }

    public void checkIfMustForgetKey()
    {
        if(name!="" && !startTimeOut)// start to count since how long we are out of the original key 
        {
            begin_time_out=Time.time;
            startTimeOut=true;
        }
        else if((Time.time-begin_time_out)>time_ignore_main) // if we are out of the original key for too long, stop remembering it 
        {
            forgetKey();
        }

    }
}
