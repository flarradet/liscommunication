using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MyButton:Button
{
    [HideInInspector]
    public int wait_color;
    [HideInInspector]

    public bool isActivated = false;

    Color mainColor;
    Color defaultColor= new Color(0,0,0,0);
    [HideInInspector]
    public Util.ButtonStatus status;
    public  bool isActivable;

    protected  override void Awake()
    {      
        base.Awake();
       status=Util.ButtonStatus.Normal;
       if(mainColor==defaultColor) //default color
            mainColor = image.color;
        updateColor();
    }

    public void updateColor()
    {
        if(status==Util.ButtonStatus.Pressed)
        {       

            image.color= new UnityEngine.Color(0.2F, 0.72F, 0.32F); // green
            wait_color = 10; // wait 10 frames as green once selected 
        }
        else
        {
            if(isActivated)
                image.color= Color.red;    
            else
                image.color=mainColor;

            if(status==Util.ButtonStatus.Hover) 
                image.color=new UnityEngine.Color(image.color.r-0.2f, image.color.g-0.2f, image.color.b-0.2f);     
        }
    }
    public void setStatus(Util.ButtonStatus status)
    {
     /*   if(status ==Util.ButtonStatus.Pressed )
        {
            //print(wait_color);
            wait_color = 1000000;
        }*/

       

        if(!(this.status== Util.ButtonStatus.Pressed && wait_color> 0 ))
        {
          this.status= status;
        }

          updateColor();           

        if(status ==Util.ButtonStatus.Pressed && isActivable){
            setIsActivated(!isActivated);
        }
    }
    public void setIsActivated(bool isActivated)
    {
       if(mainColor==defaultColor) 
            mainColor = image.color;

        this.isActivated= isActivated;
        updateColor();

    }
}
