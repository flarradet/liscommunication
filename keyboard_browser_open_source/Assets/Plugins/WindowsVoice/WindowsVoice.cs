﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class WindowsVoice : MonoBehaviour {
  [DllImport("WindowsVoice")]
  public static extern void initSpeech();
  [DllImport("WindowsVoice")]
  public static extern void destroySpeech();
  [DllImport("WindowsVoice")]
  public static extern void addToSpeechQueue( [MarshalAs(UnmanagedType.LPStr)] string s,string gender);



  public static WindowsVoice theVoice = null;
  // Use this for initialization
  void Start () {


    if (theVoice == null)
    {
      theVoice = this;
      DontDestroyOnLoad(gameObject);
      initSpeech();
    }
    //else
      //Destroy(gameObject);
  }

  public void speak(string msg, string gender) {
    print("speak");
    addToSpeechQueue(msg,gender);
  }
  void OnDestroy()
  {
    if (theVoice == this)
    {
      Debug.Log("Destroying speech");
      destroySpeech();
      theVoice = null;
    }
  }
}
