﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ItSeez3D.AvatarSdk.Core;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading;
namespace ItSeez3D.AvatarSdkSamples.Core
{
   
    public  class testPrefab : MonoBehaviour
    {
        public GameObject avatar;

        private bool isDone = false;
        //private string avatarCode = "c21de212-a55e-47a6-af8b-c232d616ca4a";
        //private string avatarCode = "ca4c93c7-8b14-47f8-8d6b-c5e2634e44f1";
        //private string avatarCode = "e18700f5-fced-4b3e-a15f-1bba6d652703";
        private string avatarCode = "629d7fdf-3e47-4703-b399-ee8120f4cc0b";


        void Start()
        {
           // happy();
                
        }



        public void happy()
        {
            print("happy");
            avatar.GetComponent<Animator>().Play("happy");
             avatar.GetComponent<Animator>().speed = 0f;

        }
        
        public void angry()
        {
            avatar.GetComponent<Animator>().Play("yawning", -1, 0.16f);
            avatar.GetComponent<Animator>().speed = 0f;

        }
        public void sad()
        {
            avatar.GetComponent<Animator>().Play("sad");
            avatar.GetComponent<Animator>().speed = 0f;
        }
        public void normal()
        {
            avatar.GetComponent<Animator>().speed = 100f;
   
        }
    }


}
