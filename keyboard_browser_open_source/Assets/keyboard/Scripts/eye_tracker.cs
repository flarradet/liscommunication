﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using UnityEngine.SceneManagement;

public class eye_tracker : Eye_tracker_interface {
 


    public GameObject[] settings ;
       [HideInInspector]

    // Use this for initialization
   public virtual void Start () {

        setup();

    }

    // Update is called once per frame
    void Update()
    {

        gazeAction();
    }


    public override void getDataFromPlayerPrefs()
    {
        if( GameObject.Find("settings"))
        {
            settings set = GameObject.Find("settings").GetComponent<settings>() ;
            time_to_wait_main = set.sp.speech.time_to_wait_main; //2.0f
            displayBall=set.sp.speech.displayBall  ;//true;
        }
        else  // if we run the browser directly, not throught the whole system 
        {
            time_to_wait_main = 1.0f;
            displayBall=false;
        }
            settings[0].GetComponent<Text>().text="main keys threshold : "+time_to_wait_main;
            if(displayBall)
                settings[1].GetComponent<Button>().GetComponent<UnityEngine.UI.Image>().color = UnityEngine.Color.red;
            else
                settings[1].GetComponent<Button>().GetComponent<UnityEngine.UI.Image>().color = UnityEngine.Color.white;
            gazePlot.SetActive(displayBall);

    }


    public override void getFinalPoint()
    {
        gazePosition =Util.calculateFixedPoint( gazePosition);
        camRay = cameraGUI.ScreenPointToRay(gazePosition);
    }

// wait a little longuer for changing thresholds and for autocompletion
public override void getTime_to_wait()
{
         if (newName.Length >= 5)
        {
                            // if it is a auto completion proposition or a threshold button or the center of the radial menu , wait longuer than normal
            if (newName.Substring(0, 5) == "propo" )
                time_to_wait = time_to_wait_main+1;
            else if (newName.Substring(0, 5) == "thres" )
                time_to_wait = Mathf.Max(time_to_wait_main,1);
            else if ( newName.Substring(0, 5) == "sleep")
                time_to_wait = Mathf.Max(time_to_wait_main,5);
            else
                time_to_wait = time_to_wait_main;

        }
        else
        {
            time_to_wait = time_to_wait_main;
        }

}

    public override void doActionButton()
    {
      


            if ((newName == "sleep" || !sleepMode) && timer.enabled)
        {

                hit.collider.gameObject.GetComponent<Button>().onClick.Invoke(); // execute the key 

                lastImage =  hit.collider.gameObject.GetComponent<MyButton>();
                lastImage.setStatus(Util.ButtonStatus.Pressed);
        }    

    }

    public override void getTime_ignore(){


        if(name2=="sleep")
        {

        }
        if ( name2== oldname )
             time_ignore= time_ignore_main+0.2f;
        else
            time_ignore = time_ignore_main;

    }

    public override void startConsideringKey()
    {
        if(newName == "sleep" || !sleepMode)
        timer.enabled = true;   
        Vector3 center = hit.collider.gameObject.transform.position;
                                    // move the position to the displayed timer to this new key 
        timer.transform.position = new Vector3(center.x , center.y , center.z);
    }

    public void changeSleepMode()
    {
           

        sleepMode = !sleepMode ;
       
    }

    public void setThresholdMain(float val)
    {
            thresholdModify(ref time_to_wait_main,settings[0].GetComponent<Text>(),val);    
    }

    public void backToMenu()
    {
                    GameObject.Find("settings").GetComponent<settings>().savePreferencesSpeech(); 
    }

    public void setDisplayBall()
    {
        displayBall = !displayBall ;
        gazePlot.SetActive(displayBall);
    }

}
