﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eye_tracker_speech : eye_tracker {

    public float time_to_wait_special;


    // Use this for initialization
  public override void Start () {
       
       		time_to_wait_special = 1;
            Util.displayCursor(true);
            setup();
    }


    public void setThresholdSpecial(float val)
    {
            thresholdModify(ref time_to_wait_special,settings[2].GetComponent<Text>(),val);    
    }


    // wait a little longuer for changing thresholds and for autocompletion
public override void getTime_to_wait()
{


            time_to_wait = time_to_wait_main;


	 if ((GameObject.Find(newName).GetComponent<MyButton>() as MyButton).GetComponent<MyButton_specialKeyboard>())

	 {
	 	time_to_wait = time_to_wait_special;
	 }

         if (newName.Length >= 5)
        {
                            // if it is a auto completion proposition or a threshold button or the center of the radial menu , wait longuer than normal
            if (newName.Substring(0, 5) == "propo" )
                time_to_wait =  Mathf.Max(time_to_wait_main+1,time_to_wait_special);
            else if (newName.Substring(0, 5) == "thres" )
                time_to_wait = Mathf.Max(time_to_wait_special,1);
            else if ( newName.Substring(0, 5) == "sleep")
                time_to_wait = Mathf.Max(time_to_wait_special,5);
        }


}

     public void closeApplication()
    {

                            #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
                             #else
        Application.Quit();
                             #endif
    }

}
