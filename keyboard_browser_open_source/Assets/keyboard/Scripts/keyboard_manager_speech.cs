using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class keyboard_manager_speech : keyboard_manager
{
    private string  emotion;
	public Text voiceBtn;
	String[] emotions = new String[] { "happy", "sad", "angrySad", "neutral" };
	public Sprite[] emotionsImagesSelected;
	public Sprite[] emotionsImagesUnselected;
	public Button[] emotionsButtons;

	String lastSentence="";
    public InputField focus;
    public GameObject menu;

    public GameObject keyboard;
    public GameObject angryBtn, happyBtn, sadBtn;
    Boolean isGSRHigh;
    float thresholdGSR;
    AudioSource audioData;
    public Text sentence;
   // private  string globalPath = "c:\\Users\\fanny\\Dropbox\\voice_stress_synthetiser\\";
  private  string globalPath;
   ///private  string globalPath = "C:\\Users\\gbarresi\\Documents\\TEEP-SLA\\voice_stress_synthetiser\\";

    void Start()
    {

        audioData = GetComponent<AudioSource>();

        string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");


        globalPath=   lines[10].Split(new string[] { "=" }, StringSplitOptions.None)[1];
     
        Time.timeScale = 1; 

    	
    	emotion = "neutral";
    	isActivated = true;

    	updateAC();

    /*	NEED TO DISABLE REPEAT 


    for(int i=3; i<6;i++)
    	{
    		specialKeys[i].GetComponent<Button>().interactable= false;
    	}*/

    	position=0;
        if (Display.displays.Length > 1)
            Display.displays[1].Activate();
    }

    void OnGUI()
    {
        Event e = Event.current;
        if (e.type == EventType.KeyDown )
        {
            if (char.IsLetter(e.keyCode.ToString()[0]) || e.keyCode == KeyCode.Space)
            {

                //This is your desired action
                print("Detected key code: " + (e.keyCode).ToString());
                WriteKey2((e.keyCode).ToString());
            }

        }



    } 

    public override void setup(){
        focus.Select();
        focus.caretPosition=position ;
    }

    public void changeEmotion(int emotionNb)
    {
    	emotion = emotions[emotionNb];
    	for (int i = 0; i < 4; i++)
    	{
    		if (i != emotionNb)
    		emotionsButtons[i].image.overrideSprite = emotionsImagesUnselected[i];
    		else
    		emotionsButtons[i].image.overrideSprite = emotionsImagesSelected[i];

    	}
    }
    
        public void SetFocus(InputField i)
        {
        	focus = i;
        }
/*
        public void SetActiveFocus(InputField i)
        {
        	focus = i;
        	SetActive(true);
        	focus.MoveTextEnd(true);
        }

            public void SetActive(bool b)
    {
        if (b)
        {
            if (!isActive)
            {
                gameObject.GetComponent<Animator>().Rebind();
                gameObject.GetComponent<Animator>().enabled = true;
            }
        }
        else
        {
            if (isActive)
            {
                gameObject.GetComponent<Animator>().SetBool("Hide", true);
            }
        }

        isActive = b;
    }
*/

        public override void WriteKey(Text t)
        {
        if (position == focus.text.Length)
        {
            focus.text += t.text;
        }
        else
        {
            focus.text = focus.text.Substring(0, position) + t.text + focus.text.Substring(position, focus.text.Length - position);
        }
        WriteKey2(t.text);
        updateAC();


    }
    void WriteKey2(string t)
    {

        position++;
        focus.caretPosition = position;


    }
    /*
            public void WriteSpecialKey(int n)
            {

                if (n == 14 || isActivated)
                {
                    float threshold;
                    switch (n)
                    {
                    case 0: // delete letter 
                        deleteLetter();
                        break;
                    case 1:  // play 
                        say(focus.text);
                        break;
                    case 2:
                        SwitchCaps();
                        break;
                    case 3:
                        SetActive(false);
                        break;
                    case 4:
                        SetKeyboardType(1);
                        break;
                    case 5:
                        SetKeyboardType(2);
                        break;
                    case 6: // previous letter 
                        if(position>0)
                        {
                            position --;
                            focus.caretPosition=position ;
                            updateAC();
                        }
                        break;
                    case 7: //next position
                        if(position!= focus.text.Length)
                        {
                            position ++;
                            focus.caretPosition=position ;                        
                            updateAC();
                        }
                        break;
                    case 8:
                        SetKeyboardType(0);
                        break;
                    case 9: //threshold
                        threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main + 0.25f;
                        GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                        GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                        break;
                    case 10: // threshold
                        threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main - 0.25f;
                        threshold = Math.Max(0.25f, threshold);
                        GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                        GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                        break;
                    case 11: // change gender
                        if (gender == "Male")
                        {
                            gender = "Female";
                            voiceBtn.text = "Femminile";
                        }
                        else
                        {
                            gender = "Male";
                            voiceBtn.text = "Maschio";
                        }
                        break;
                    case 12: // display/hide ball

                        GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall= !GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall;
                        if(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall)
                            GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.red;
                        else
                            GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.white;
                         GameObject.Find("eyeTracker").GetComponent<eye_tracker>().gazePlot.SetActive(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall);

                        break;
                    case 13: // clear 
                        clear();
                        break;
                    case 14: // sleep mode 
                        //isActivated = !isActivated;
                        for (int i=0;i< 3; i++)
                        {
                            specialKeys[i].GetComponent<Button>().GetComponent<Image>().color = buttonColors[Convert.ToInt32(!isActivated)];
                        }

                        break;
                    case 15: // delete word
                        deleteWord();
                        break;
                    case 16: //repeat
                        say(lastSentence);
                        break;

                }
            }
          //  remplaceText();
        }
    */

    public void say(string text)
    {
        
            // NEED  ENABLE REPEAT 
            
        print("globalpath is:" + globalPath);

        GameObject.Find("Key 34").GetComponent<Button>().interactable = true;


    	lastSentence= text ;
        sentence.text = lastSentence;
   
        string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
        string type = lines[2].Split(new string[] { "=" }, StringSplitOptions.None)[1];
        string language = lines[3].Split(new string[] { "=" }, StringSplitOptions.None)[1];
        string languageEmotional = lines[6].Split(new string[] { "=" }, StringSplitOptions.None)[1];
        bool emotionalVoice = true;
        print("emotionalVoice is"+emotionalVoice);
        print("type is"+type);
        print("language is"+language);
        print("languageEmotional is"+languageEmotional);
        print("text is" + text);

        if (!emotionalVoice)
        {
            sayNoEmotion(type,language,text);  
        }
        else
        {
            
           // String languageName = "us2";
             String languageName = languageEmotional;

            string localisation_mbrola = globalPath+"voices_mbrola\\";
            print("/C  echo '" + text + "' | espeak -v mb-" + languageName + " --pho --phonout=test/mypho.pho");

            String emotionRes = emotion;
              if(emotion == "neutral" && isGSRHigh)
            {
                emotionRes = "highPitch";
            }

            if(emotionRes == "neutral")
            {
                print("neutral");
                 sayNoEmotion( type, language,text);
            }
            else{
                
                print("begin pholist");
                createPhoList(languageName,text);
                print("end pholist");
                
                doCommand(@"C:\Program Files (x86)\eSpeak\command_line\espeak.exe", "-q -v mb-" + languageName + " --pho --phonout=test/mypho.pho \"" + text+"\"");
                
              //  doCommand(@"C:\Program Files (x86)\Common Files\Oracle\Java\javapath\javaw.exe", "-jar emofilt.jar -cf emofiltConfig_windows.ini -if test/mypho.pho -voc " + languageName + " -e " + emotion + " -of tmp_e.pho");
                print( "-jar emofilt.jar -cf emofiltConfig_windows.ini -if test/mypho.pho -voc " + languageName + " -e " + emotionRes + " -of tmp_e.pho");
                doCommand(@"javaw", "-jar emofilt.jar -cf emofiltConfig_windows.ini -if test/mypho.pho -voc " + languageName + " -e " + emotionRes + " -of tmp_e.pho");
                
                doCommand(@"phoplayer.exe", " database=" + localisation_mbrola + languageName + "\\" + languageName + " tmp_e.pho");
                
            }
            
        }
        

    }
    public void sayNoEmotion(string type,string language,string text)
    {
                 if (type == "Windows voice")
            {

                print("windows voice");
                print("Name=" + language);
                GameObject.Find("speech").GetComponent<WindowsVoice>().speak(text, "Name=" + language);

            }
            else
            {
                
                Process proc1 = new Process();
                proc1.StartInfo.FileName = @"C:\Program Files (x86)\eSpeak\command_line\espeak.exe";
                proc1.StartInfo.UseShellExecute = false;

                proc1.StartInfo.CreateNoWindow= true;
                proc1.StartInfo.Arguments = "-v "+language+" \""+text+"\"";
                proc1.Start();
          
            }
    }
    
    public string doCommand(string cmd, string arguments)
    {
        System.Diagnostics.Process p = new System.Diagnostics.Process();
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.CreateNoWindow = true;


        // Correct way to launch a process with arguments
        p.StartInfo.FileName = cmd;
        p.StartInfo.Arguments = arguments;
        p.StartInfo.WorkingDirectory = globalPath+"Emofilt-master\\emofilt";
        p.Start();

        string q = "";
        while (!p.HasExited)
        {
            q += p.StandardOutput.ReadToEnd();
        }
        return (q);
    }

    public void createPhoList(string languageName,string text)
    {
        
        String path = globalPath+"Emofilt-master\\emofilt\\";

        string firstLine ="";
        string[] words = text.Split(' ');
        string phoneme = path + "test/mypho.pho";
        String phoFile = path + "phoList.txt";
        List<String> strList = new List<String>();

        for (int wInt=0; wInt< words.Length ; wInt++)
        {
            if(words[wInt] !="")
            {
                string res= doCommand(@"C:\Program Files (x86)\eSpeak\command_line\espeak.exe", "-q -v mb-" + languageName + " --pho \"" + words[wInt]+"\"");
                string[] lines = res.Split('\n');
            int count =0;

                foreach ( string line in lines)
            {
                if(line.Split('\t')[0]!="_"){
                            strList.Add((line.Split('\t')[0]).Split('\n')[0] + ";");


                        count =count+1;
                }
            }
                strList.RemoveAt(strList.Count - 1);
                count = count - 1;
 

            strList[strList.Count-1]=strList[strList.Count-1].Substring(0,strList[strList.Count-1].Length-1);

                strList.Add("\n");

                if(wInt== words.Length-1)
                    count = count+2;
                firstLine=firstLine+count+";";

            }
        }
            strList.RemoveAt(strList.Count-1);
        strList.RemoveAt(strList.Count - 1);
        strList.Add(";_;_");
        

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(phoFile, false);
            writer.Write(firstLine.Substring(0,firstLine.Length-1)+"\n");

            for (int i = 0; i < strList.Count; i++)
            {
                writer.Write(strList[i]);
            }

        writer.Close();
        
        
    }
    public override void writeWord(string text)
    {

    //	if (isActivated)
    //	{

    		if(focus.text=="")
    		{
    			focus.text= text;
    			position = text.Length+1;

    			focus.text = focus.text+" ";
    			focus.caretPosition=position ;


        	}
            else{
            	String focusedWord =	ac.getFocusedWord(focus.text,position);

            	focus.text= focus.text.Substring(0, position - focusedWord.Length )+ text + focus.text.Substring(position,focus.text.Length - position);
            	position= position + text.Length - focusedWord.Length;
            	if(focus.text.Length==position ||  focus.text[position]!=' ')
            	{    focus.text = focus.text+" ";
            	position++;
                }
     //   }

    	focus.caretPosition=position ;

        updateAC();
        
    }
      //  remplaceText();
}
    /*
    public void remplaceText(){

		if(focus.text!="")
		{
			focus.text= focus.text;
		
			if(focus.text[focus.text.Length -1]== ' ')
			{
				focus.text= focus.text.Substring(0, focus.text.Length - 1) + "_";
			}
		}
		}*/
		public override void deleteLetter()
		{
			if (!focus) { return; }
			if (focus.text.Length > 0)
			{
                        //focus.text = focus.text.Substring(0, focus.text.Length - 1);
				focus.text= focus.text.Substring(0, position-1 )+ focus.text.Substring(position,focus.text.Length - position);
				position --;

			}
    		focus.caretPosition=position ;

			updateAC();

		}
		public override void deleteWord()
		{
			if(focus.text!="")
			{
				String[] words = focus.text.Split(' ');
				int cpt=0;
				int j=0;
				while(j<words.Length && cpt < position)
				{
					cpt=cpt+words[j].Length;
                    if(j!=0) //count the space in between words
                    cpt++;
                    j++;
                }
                int toRemove;
                if(j>0)
                {
                	toRemove= words[j-1].Length;

            // if the focus character is a space, delete it and the last word
                	if(toRemove ==0)
                	{
                		deleteLetter();
                		deleteWord();
                	}else{

                		focus.text= focus.text.Substring(0, cpt-toRemove )+ focus.text.Substring(cpt,focus.text.Length-cpt );

          //  focus.text = focus.text.Substring(0, focus.text.Length  - toRemove);
                		position = cpt - toRemove;
                	}

                }
                focus.caretPosition=position ;

                updateAC();

            }
        }

        public override void clear()
        {
        	focus.text = "";
        	position =0;
           	focus.caretPosition=position ;

        	updateAC();

        }
    public void backToMenu()
    {
    	print("back to menu");
        //Application.LoadLevel("menu"); 
        SceneManager.LoadScene("menu");

        GameObject.Find("settings").GetComponent<settings>().savePreferencesSpeech(); 
    }
   
   public override void arrows(string direction){

        switch(direction) {
            case "left":
                    if(position>0)
                    {
                        position --;
                        focus.caretPosition=position ;
                        updateAC();
                    }
            break;
            case "right":
                    if(position!= focus.text.Length)
                    {
                        position ++;
                        focus.caretPosition=position ;                        
                        updateAC();
                    }
            break;
        }
   }
   public override void send(){ 
        say(focus.text);
            clear();
    }

    public override void updateAC()
    {
                ac.updateProposals(focus.text,position);
    }

    public void displayMenu()
    {
            menu.SetActive(!menu.active);
            keyboard.SetActive(!menu.active);
          focus.gameObject.SetActive(!menu.active);


    }

   
    public void repeat()
    {
        print("repeat "+ lastSentence);
        say(lastSentence);
    }


    public void happy()
    {
        if (emotion == "joy")
        {
            emotion = "neutral";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 100f;
        }
        else
        {
            emotion = "joy";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().Play("happy");
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 0f;
        }

        angryBtn.GetComponent<MyButton>().setIsActivated(false);
        sadBtn.GetComponent<MyButton>().setIsActivated(false);
       // GameObject.Find("neutral").GetComponent<MyButton>().setIsActivated(false);

    }

    public void angry()
    {
        if (emotion == "coldAnger")
        {
            emotion = "neutral";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 100f;

        }
        else
        {
            emotion = "coldAnger";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().Play("angry", -1, 0.16f);
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 0f;
        }

        happyBtn.GetComponent<MyButton>().setIsActivated(false);
        sadBtn.GetComponent<MyButton>().setIsActivated(false);
       // GameObject.Find("neutral").GetComponent<MyButton>().setIsActivated(false);
    }
    public void sad()
    {
        if (emotion == "sad")
        {
            emotion = "neutral";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 100f;

        }
        else
        {
            emotion = "sad";
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().Play("sad");
            GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 0f;
        }

        angryBtn.GetComponent<MyButton>().setIsActivated(false);
        happyBtn.GetComponent<MyButton>().setIsActivated(false);
      //  GameObject.Find("neutral").GetComponent<MyButton>().setIsActivated(false);
    }
    public void normal()
    {

        emotion = "neutral";
        GameObject.Find("avatar").transform.Find("ItSeez3D Head").GetComponent<Animator>().speed = 100f;
        GameObject.Find("angry").GetComponent<MyButton>().setIsActivated(false);
        GameObject.Find("sad").GetComponent<MyButton>().setIsActivated(false);
        GameObject.Find("happy").GetComponent<MyButton>().setIsActivated(false);

    }
   
    public void laugh()
    {
        audioData.Play(0);
    }

}
