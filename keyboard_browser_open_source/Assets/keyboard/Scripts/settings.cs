﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json ;
using System.IO;
using UnityEngine.SceneManagement;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine.SceneManagement;


public class settings : MonoBehaviour {
	public savePrefs sp;
      [HideInInspector]
  public GameObject browserObject;
    private bool isHRhigh;
    private float thresholdHR;
    private List<(DateTime, float)> lasts = new List<(DateTime,float)>();
    public DateTime lastTimeUsedTracker, lastTimeSentEmail;

    public GameObject  myPrefab;


    // Use this for initialization
    void Start () {
              lastTimeSentEmail= DateTime.Now;
        lastTimeUsedTracker= DateTime.Now;

        DontDestroyOnLoad(this.gameObject);
		loadPreferences();

        isHRhigh = false;
        string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
        thresholdHR = (float)Convert.ToDouble(lines[18].Split(new string[] { "=" }, StringSplitOptions.None)[1]);


    }
    void Update()
    {
     // print(lastTimeUsedTracker);
    }

    public void savePreferencesBrowser()
    {
       
    } 
 
        public void savePreferencesSpeech()
    {
    	  speechPrefs speech= new speechPrefs();
       	eye_tracker ey= GameObject.Find("eyeTracker").GetComponent<eye_tracker>();
        speech.time_to_wait_main =ey.time_to_wait_main;
        speech.displayBall=ey.displayBall;
        sp.speech = speech;
        //Application.LoadLevel("menu");
        SceneManager.LoadScene("menu");

    } 
            public void savePreferences()
    {
       // browser.localStorage=null;
        string json = JsonConvert.SerializeObject(sp);


        StreamWriter writer = new StreamWriter("playerPrefs.txt", false);
        writer.WriteLine(json);
        writer.Close();

    }
        public void loadPreferences()
    {
      if(sp == null) // if we havent already load the preferences 
      {
        try
        {
      		string json;
          StreamReader reader = new StreamReader("playerPrefs.txt"); 
          json= reader.ReadToEnd();
          reader.Close();

           sp = JsonConvert.DeserializeObject<savePrefs>(json);
         }catch(Exception e)
         {        

            createDefault();
         }
         if( sp.offsets == null)
         {
           // Application.LoadLevel("calibration");
                  SceneManager.LoadScene("calibration");

         }
         else
         {
                //   Application.LoadLevel("menu");
                  SceneManager.LoadScene("menu");

   
         }
       }
    } 
    public void createDefault()
    {
      print("createDefault");
         sp = new savePrefs();
        browserPrefs b = new browserPrefs();
        speechPrefs s = new speechPrefs();
        b.time_to_wait_main=1.0f;
        b.time_to_wait_menu = 1f;
        b.time_to_wait_radialButtons=1.0f;
        b.displayBall=false;
        b.radius_menu=15f;

        s.time_to_wait_main=1.0f;
        s.displayBall=false;

        sp.browser=b;
        sp.speech = s;
        sp.offsets =  new Vector2[5]{new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0)};
        sp.meanOffsets = new Vector2(0,0);

    }


public class savePrefs {
   public browserPrefs browser;
   public speechPrefs speech;
   public Vector2[] offsets;
   public Vector2 meanOffsets;
}

public class browserPrefs {
   public float time_to_wait_main;
   public float time_to_wait_menu;
   public float time_to_wait_radialButtons;
   public  float radius_menu;
   public  bool displayBall;
   public  Dictionary<string,List<KeyValuePair<string,string>> >  localStorage;

}

public class speechPrefs {
   public float time_to_wait_main;
   public float time_to_wait_autocompletion;
   public bool displayBall;
}


    private void SendEmail()
    {
      if( (DateTime.Now - lastTimeSentEmail ).TotalSeconds > 3600)
      {
        string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
        string subject = lines[13].Split(new string[] { "=" }, StringSplitOptions.None)[1];
        string body = lines[14].Split(new string[] { "=" }, StringSplitOptions.None)[1];
        string recipiant = lines[15].Split(new string[] { "=" }, StringSplitOptions.None)[1];

        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("teepsla@gmail.com");
        mail.To.Add(recipiant);
        mail.Subject = subject;
        mail.Body = body;

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential("teepsla@gmail.com", "teepslapsw") as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
            return true;
        };
        smtpServer.Send(mail);
        Debug.Log("success");
        lastTimeSentEmail= DateTime.Now;
      }
    }
    private float getLastsMean()
    {
        int i = 0;
        float mean = -1f;
        while ( i< lasts.Count &&  DateTime.Now.Subtract(lasts[i].Item1).Seconds > 3*60)
        {
            lasts.RemoveAt(0);
        }
        foreach ((DateTime,float) value in  lasts )
        {
            mean = mean + value.Item2; 
        }
        if( mean ==-1)
          return mean;
        else
          return  mean / lasts.Count;
        
    }
    private void detectHr()
    {


        print("hr entre displaySentences");
        print("hr displaySentences lastTimeUsedTracker"+( DateTime.Now - lastTimeUsedTracker).TotalSeconds) ;


          // if at the moment of the event, he hasnt used in the last 30 min, prompted sentence
          if(( DateTime.Now - lastTimeUsedTracker).TotalSeconds > 30*60 )
          {
          displaySentences();
                    
            
          }
    }

    public void displaySentences()
    {
         print("display sentence");
         lastTimeUsedTracker = DateTime.Now;
          SceneManager.LoadScene("sentence");
    }
}
