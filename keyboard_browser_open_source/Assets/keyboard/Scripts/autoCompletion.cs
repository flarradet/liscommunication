﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.UI;

public class autoCompletion : MonoBehaviour {


    //[DllImport("libface.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
  //  public static extern void getResults([MarshalAs(UnmanagedType.LPStr)] string entry, StringBuilder res);

        [DllImport("libface.dll" , EntryPoint = "getResults")]
        private static extern IntPtr getResults([MarshalAs(UnmanagedType.LPStr)] string entry);

    [DllImport("libface")]
    public static extern void import();
    public Text[] proposals;


    // Use this for initialization
    void Start () {

        import();
        
    }

    public virtual void updateProposals(string text,int position)
    {
      print("update proposals");
       // text= text;
        if (text == "")
        {
           string[] lines = System.IO.File.ReadAllLines(@"totalList.tsv");
            print("ici");
     
  
            for(int i=0;i< 6;i++)
            {
              
                proposals[i].text =  lines[i].Split(new string[] { "	" }, StringSplitOptions.None)[1];            
            }


                        // if its the letter keyboard that is activated 
            if(GameObject.Find("keyboard").GetComponent<keyboard_manager>().panels[0].activeSelf)
            {
                checkPossibleLetters("");
            }   
        }
        else { 

          //  print("position="+position);
           // print("length="+text.Length);
            if(position == text.Length || text[position]==' ' )
            {
               String focusedWord = getFocusedWord(text,position);

      print("avant import");


          //  print("focusedWord"+ focusedWord);
              //  StringBuilder result = new StringBuilder(2000);

                import();
                      print("apres import");

               // String lastWord = words[words.Length - 1];
               // getResults(focusedWord, result);
               string result= getRes(focusedWord);
                     print("result="+result);

                string tmp = result.ToString().Split('[')[2];
                string[] res = tmp.Split('"');
                int i = 0;

                while (i * 2 + 1 < res.Length &&  i< 6)
                {
                    proposals[i].text = res[i * 2 + 1];
                    proposals[i].transform.parent.GetComponent<Button>().interactable =true;
                    i++;
                }
                while(i<6)
                {
                    proposals[i].text = "";
                    proposals[i].transform.parent.GetComponent<Button>().interactable =false;
                    i++;
                }
            // if its the letter keyboard that is activated 
                if(GameObject.Find("keyboard").GetComponent<keyboard_manager>().panels[0].activeSelf)
                {

                    checkPossibleLetters(focusedWord);
                }   
            }
            else
            {
                for(int i=0;i<6;i++)
                {
                    proposals[i].text = "";
                    proposals[i].transform.parent.GetComponent<Button>().interactable =false;
                }
                for(int i=97; i<123;i++)
                {
                    GameObject.Find("Key "+(String)(i-96).ToString()).GetComponent<Button>().interactable =true;
                }
            }
        }
    }
    public void checkPossibleLetters(string lastWord)
    {          
      //StringBuilder result = new StringBuilder(2000);
        /*
        string result;
      import();
      // check if the letter is possible, if not grey it 
      for(int i=97; i<123;i++)
      {
       // result = new StringBuilder(2000);

       // getResults(lastWord+(char)i, result);
         result= getRes(lastWord+(char)i);
         print("result for letter "+ (char)i+" "+result);
        char tmp = result.ToString().Split('[')[2][0];

        if(tmp==']')
        {
            GameObject.Find("Key "+(String)(i-96).ToString()).GetComponent<Button>().interactable =false;
        }
        else
        {
                 
            GameObject.Find("Key "+(String)(i-96).ToString()).GetComponent<Button>().interactable =true;
        }
    }*/
        
        string result;
      import();
      // check if the letter is possible, if not grey it 
      for(int i=1; i<27;i++)
      {
        GameObject go = GameObject.Find("Key "+i);
       

        
        char let = go.GetComponent<Button>().GetComponentInChildren<Text>().text[0];
           // result = new StringBuilder(2000);

           // getResults(lastWord+(char)i, result);
             result= getRes(lastWord+let);
            char tmp = result.ToString().Split('[')[2][0];
 
            if(tmp==']')
            {
                go.GetComponent<Button>().interactable =false;
            }
            else
            {
                     
               
               go.GetComponent<Button>().interactable =true;
            }
          
        }


}
public String getFocusedWord(String text, int position)
{
                String[] words = text.Split(' ');
                int cpt=0;
                int j=0;
                while(j<words.Length && cpt < position)
                {
                    cpt=cpt+words[j].Length;
                    if(j!=0) //count the space in between words
                        cpt++;
                    j++;
                }
                if(j>0)
                    return words[j-1];
                else
                    return words[0];

}

public string getRes(string request)
{

var data = new List<byte>();
var ptr = getResults(request);
var off = 0;
while (true)
{
  var ch = Marshal.ReadByte(ptr, off++);
  if (ch == 0)
  {
    break;
  }
  data.Add(ch);
}
string sptr = Encoding.UTF8.GetString(data.ToArray());
return sptr;
}
}
