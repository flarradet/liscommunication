﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Microsoft.VisualBasic;
using System.Diagnostics;
namespace setLanguageWindowsVoice
{
    public partial class Form1 : Form
    {
        List<string> languages = new List<string>();
        List<string> types = new List<string>();

        List<List<List<string>>> voices = new List<List<List<string>>>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.SelectedValueChanged += new EventHandler(listBox1_SelectedIndexChanged);
            listBox2.SelectedValueChanged += new EventHandler(listBox1_SelectedIndexChanged);
            listBox3.SelectedValueChanged += new EventHandler(listBox3_SelectedIndexChanged);
            button2.Click += new EventHandler(choose);

                        button2.Enabled = false;

            // windows voices 
            try
            {


                using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Speech\Voices\Tokens"))
                {


                    if (!(registryKey == null))
                    {


                        string[] temp = registryKey.GetSubKeyNames();

                        Console.WriteLine(temp);
                        foreach (string str in temp)
                        {
                            using (RegistryKey voice = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Speech\Voices\Tokens\" + str))
                            {
                                string[] n = voice.GetValueNames();
                                string nameVoice = (voice.GetValue(n[1])).ToString().Split('-')[1];

                                int index = languages.IndexOf(nameVoice);

                                if (index == -1)    //!languages.Contains()
                                {
                                    List<List<string>> l = new List<List<string>>();
                                    l.Add(new List<string>()); //male
                                    l.Add(new List<string>()); //female

                                    languages.Add(nameVoice);
                                    types.Add("Windows voice");
                                    addTo(l, str);
                                    voices.Add(l);

                                }
                                else
                                {

                                    addTo(voices[index], str);
                                }
                            }
                        }
                    }

                }
            }

            catch
            {

            }
            listBox2.Items.Add("Male");
            listBox2.Items.Add("Female");



            //espeak voices 

            Process proc1 = new Process();
            proc1.StartInfo.FileName = @"C:\Program Files (x86)\eSpeak\command_line\espeak.exe";
            proc1.StartInfo.UseShellExecute = false;
            proc1.StartInfo.CreateNoWindow = true;
            proc1.StartInfo.RedirectStandardOutput = true;
            proc1.StartInfo.Arguments = "--voices";
            proc1.Start();
            proc1.StandardOutput.ReadLine(); // dont consider first line titles

            while (!proc1.StandardOutput.EndOfStream)
            {
                string line = proc1.StandardOutput.ReadLine();
                Console.WriteLine(line);
                string[] parts2 = System.Text.RegularExpressions.Regex.Split(line, @" ");
                List<string> partFinal = new List<string>();
                foreach (string p in parts2)
                {
                    if (System.Text.RegularExpressions.Regex.Matches(p, " ").Count != p.Length)
                    {
                        partFinal.Add(p.Replace(" ", ""));
                        Console.WriteLine("ici p)" + p);

                    }
                }

                languages.Add(partFinal[3]);


                List<List<string>> l = new List<List<string>>();
                l.Add(new List<string>()); //male
                l.Add(new List<string>()); //female

                string code = "";
                // get the code of the language7
                if (partFinal[4].Contains('\\'))
                    code = partFinal[4].Split('\\')[1];
                else if (partFinal[4] == "default")
                    code = "en";
                else
                    code = partFinal[4];


                l[0].Add(code);
                for (int i=1;i<6;i++)
                {
                    l[0].Add(code+"+m"+i);
                    l[1].Add(code + "+f" + i);
                }
                l[0].Add(code + "+m6");
                l[0].Add(code + "+m7");

                voices.Add(l);
                types.Add("Espeak");


            }


            for (int i = 0; i < languages.Count; i++)
            {
                listBox1.Items.Add(languages[i]);
            }


        }


        private void addTo(List<List<string>> l, string str)
        {
            using (RegistryKey tempKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\" + str + "\\Attributes"))
            {
                  
                      string name=  tempKey.GetValue("Name").ToString();
                string gender = tempKey.GetValue("Gender").ToString();

                if(gender=="Male")
                    l[0].Add(name);
                else
                    l[1].Add(name);
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            label5.Text = "";
            button2.Enabled = false;
            label4.Text = "";
            listBox3.Items.Clear();
            int languageIndex = listBox1.SelectedIndex;
            int genderIndex = listBox2.SelectedIndex;
            if (languageIndex != -1 && genderIndex != -1)
            {
                if (voices[languageIndex][genderIndex].Count == 0)
                {
                    listBox3.Items.Add("no available windows voice with those choices, you may use an espeak voice from the list instead )");
                }
                else
                {
                    for (int i = 0; i < voices[languageIndex][genderIndex].Count; i++)
                    {
                        listBox3.Items.Add(voices[languageIndex][genderIndex][i]);

                    }
                }
            }
            if (languageIndex != -1)
            {
                label4.Text = types[languageIndex];
            }
        }
            private void listBox3_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            button2.Enabled = true;
        }

        private void choose(object sender, System.EventArgs e)
        {



            // string[] lines  = System.IO.File.ReadAllLines("../../../../../language.txt");
             string[] lines  = System.IO.File.ReadAllLines("preferences.txt");

           
            lines[3] = "type="+ types[listBox1.SelectedIndex];
            

            lines[4] = "language="+listBox3.SelectedItem.ToString();

            // System.IO.File.WriteAllLines("../../../../../language.txt", lines);
             System.IO.File.WriteAllLines("preferences.txt", lines);
            label5.Text = "Voice succesfully chosen";
        }


    }
}
