To simply use the system: 

In builds: 

modify the path in preferences.txt
run New Unity Project.exe


To modify the system :


Modidy the path in keyboard_browser_open_source/preferences.txt
Modify all paths in voice_stress_synthetiser/Emofilt-master/emofiltConfig_windows.ini


to modify the avatar: 
1/ set up your avatarSDK credentials (see https://avatarsdk.com/ )
2/ run keyboard_browser_open_source\Assets\avatar\itseez3d\avatar_sdk\samples_cloud\01_getting_started_sample_cloud\scenes\01_getting_started_sample_cloud.unity
  Upload you image 
  Create an avatar 
3/ run keyboard_browser_open_source\Assets\avatar\itseez3d\avatar_sdk\samples_cloud\02_gallery_sample_cloud\scenes\02_gallery_sample_cloud.unity
   Change the haircut
   Create a prefab
4/ Run keyboard_browser_open_source\Assets\keyboard\keyboard.unity 
   add the newly created prefab 
   copy the parameter from the old avatar prefab
   delete the old avatar prefab
   rename the new avatar "avatar"


